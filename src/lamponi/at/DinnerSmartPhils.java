package lamponi.at;

public class DinnerSmartPhils extends Dinner {
    DinnerSmartPhils(int numberOfPhilosophers, int thinkingTime, int eatingTime, int roundTillStarvation) {
        super(numberOfPhilosophers, thinkingTime, eatingTime, roundTillStarvation);
    }

    protected void prepare() {
        for (int i = 0; i < numberOfPhilosophers; i++)
        {
            forks.add(new ForkSynchronized(i));
        }

        for(int i = 0; i < numberOfPhilosophers; i++)
        {
            String name = String.format("phil%s", i);
            phils.add(new SmartPhilosopher(name, forks.get(i), forks.get((i+1) % numberOfPhilosophers), thinkingTime, eatingTime, roundTillStarvation, i));
        }

        for (Philosopher phil: phils) {
            phil.printForks();
        }
    }
}
