package lamponi.at;

import java.util.ArrayList;
import java.util.List;

public class Dinner {

    int numberOfPhilosophers;
    int thinkingTime;
    int eatingTime;
    int roundTillStarvation;

    private int avgThinking = 0;
    private int avgEating = 0;
    private long elapsedTime = 0;

    List<Philosopher> phils = new ArrayList<>();
    List<Fork> forks = new ArrayList<>();
    private List<Thread> threads = new ArrayList<>();

    Dinner(int numberOfPhilosophers, int thinkingTime, int eatingTime, int roundTillStarvation)
    {
        this.numberOfPhilosophers = numberOfPhilosophers;
        this.thinkingTime = thinkingTime;
        this.eatingTime = eatingTime;
        this.roundTillStarvation = roundTillStarvation;
    }

    public long getElapsedTime()
    {
        return elapsedTime;
    }

    public void run()
    {
        this.prepare();

        long startTime = System.currentTimeMillis();

        thinkAndEat();

        closeThreads();

        long stopTime = System.currentTimeMillis();
        elapsedTime = stopTime - startTime;
        System.out.println(elapsedTime);
        printResult(isAnyoneDead());
    }

    protected void prepare() {
        for (int i = 0; i < numberOfPhilosophers; i++)
        {
            forks.add(new Fork(i));
        }

        for(int i = 0; i < numberOfPhilosophers; i++)
        {
            String name = String.format("phil%s", i);
            phils.add(new Philosopher(name, forks.get(i), forks.get((i+1) % numberOfPhilosophers), thinkingTime, eatingTime, roundTillStarvation));
        }

        for (Philosopher phil: phils) {
            phil.printForks();
        }
    }

    private void thinkAndEat()
    {
        for(Philosopher phil : phils)
        {
            Thread t = new Thread(phil);
            threads.add(t);
            t.start();
        }
    }

    private boolean isAnyoneDead() {
        for (Philosopher phil : phils) {
            if (phil.isDead())
            {
                return true;
            }
        }
        return false;
    }

    private void closeThreads()
    {
        for(Thread t : threads)
        {
            try {
                t.join();
            } catch (InterruptedException e) {
                for(Thread th : threads)
                {
                    th.interrupt();
                }
                for(Philosopher phil : phils)
                {
                    phil.setStop(true);
                }
            }
        }
    }

    private void printResult(boolean someoneDied)
    {
        for(Philosopher phil : phils)
        {
            avgThinking += phil.getTimesSpentThinking();
            avgEating += phil.getTimesSpentEating();
        }

        System.out.println("on average each philosopher spent " + Math.ceil(avgThinking/phils.size()) + " times thinking");
        System.out.println("on average each philosopher spent " + Math.ceil(avgEating/phils.size()) + " times eating");

        if(someoneDied)
        {
            System.out.println("sadly there was a deadlock and someone died");
        }
    }
}
