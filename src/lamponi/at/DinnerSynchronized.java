package lamponi.at;

public class DinnerSynchronized extends Dinner {

    DinnerSynchronized(int numberOfPhilosophers, int thinkingTime, int eatingTime, int roundTillStarvation) {
        super(numberOfPhilosophers, thinkingTime, eatingTime, roundTillStarvation);
    }

    protected void prepare() {
        for (int i = 0; i < numberOfPhilosophers; i++)
        {
            forks.add(new ForkSynchronized(i));
        }

        for(int i = 0; i < numberOfPhilosophers; i++)
        {
            String name = String.format("phil%s", i);
            phils.add(new Philosopher(name, forks.get(i), forks.get((i+1) % numberOfPhilosophers), thinkingTime, eatingTime, roundTillStarvation));
        }

        for (Philosopher phil: phils) {
            phil.printForks();
        }
    }
}
