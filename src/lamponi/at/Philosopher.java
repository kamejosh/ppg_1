package lamponi.at;

public class Philosopher implements Runnable{

    private String name;
    boolean hasForkLeft = false;
    boolean hasForkRight = false;
    private boolean hungry = false;
    private boolean dead = false;
    private boolean stop = false;
    private int roundsSpentHungry = 0;
    private int timesSpentThinking = 0;
    private int timesSpentEating = 0;
    Fork forkLeft;
    Fork forkRight;
    private int thinkingTime;
    private int eatingTime;
    private int roundsTillStarvation;

    private long startTime;
    private long stopTime;
    private long waitForFork;


    public void setStop(boolean stop)
    {
        System.out.println(this.name + " was stopped from the outside!");
        this.stop = stop;
    }

    public boolean isDead()
    {
        return this.dead;
    }

    private boolean isHungry()
    {
        return this.hungry;
    }

    public String getName()
    {
        return this.name;
    }

    public int getTimesSpentThinking()
    {
        return this.timesSpentThinking;
    }

    public int getTimesSpentEating()
    {
        return this.timesSpentEating;
    }

    private void setHungry(boolean hungry)
    {
        String isHungry = hungry ? "" : "not ";
        System.out.println(this.name + " is " + isHungry + "hungry!");
        this.hungry = hungry;
    }

    Philosopher(String name)
    {
        this.name = name;
    }

    Philosopher(String name, Fork forkLeft, Fork forkRight, int thinkingTime, int eatingTime, int roundsTillStarvation)
    {
        this.name = name;
        this.forkLeft = forkLeft;
        this.forkRight = forkRight;
        this.thinkingTime = thinkingTime;
        this.eatingTime = eatingTime;
        this.roundsTillStarvation = roundsTillStarvation;
    }

    private void think() throws InterruptedException {
        System.out.println(name + " is thinking!");
        timesSpentThinking++;
        sleep(thinkingTime);
        this.setHungry(true);
    }

    private void eat() throws InterruptedException {
        startTime = System.currentTimeMillis();
        this.pickUpForks();
        stopTime = System.currentTimeMillis();

        waitForFork += stopTime - startTime;

        if(hasForkLeft && hasForkRight) {
            System.out.println(name + " is eating!");
            roundsSpentHungry = 0;
            timesSpentEating++;
            sleep(eatingTime);
            setHungry(false);
            try {
                hasForkLeft = forkLeft.put(this);
                hasForkLeft = forkRight.put(this);
            } catch(RuntimeException e)
            {
                this.dead = true;
                System.out.println(this.name + " died because of a runtime exception: " + e.toString());
            }
        }
        else
        {
            roundsSpentHungry++;
            System.out.println(name + " is still hungry!");
        }

        if(roundsSpentHungry > this.roundsTillStarvation)
        {
            System.out.println(name + " died of hunger!");
            dead = true;
        }
    }

    public void printForks()
    {
        System.out.println(this.name + " has fork " + forkLeft.getID() + " at his left hand and " + forkRight.getID() + " at his right hand.");
    }

    @Override
    public void run() {
        while(!dead && !stop)
        {
            try{
                if(!this.isHungry())
                {
                    this.think();
                }
                else
                {
                    this.eat();
                }
            } catch( InterruptedException e)
            {
                System.out.println(this.name + "caught an interrupt: " + e.toString());
            }
        }
        System.out.println(this.name + "spent " + waitForFork + "ms waiting for a Fork");
        Thread.currentThread().interrupt();
    }

    private void sleep (int maxTime) throws InterruptedException {
        long sleepTime = (long)(Math.random() * maxTime);
        Thread.sleep(sleepTime);
    }

    protected void pickUpForks() throws InterruptedException {
        if(!hasForkLeft)
        {
            hasForkLeft = forkLeft.take(this);
        }

        if(!hasForkRight && hasForkLeft)
        {
            hasForkRight = forkRight.take(this);
        }
    }
}
