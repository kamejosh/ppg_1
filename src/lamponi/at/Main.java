package lamponi.at;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("number of philosophers: ");
        int numberOfPhilosophers = sc.nextInt();
        //int numberOfPhilosophers = 5;
        System.out.println();

        System.out.print("max thinking time of philosophers: ");
        int thinkingTime = sc.nextInt();
        //int thinkingTime = 0;
        System.out.println();

        System.out.print("max eating time of philosophers: ");
        int eatingTime = sc.nextInt();
        //int eatingTime = 0;
        System.out.println();

        long startTime = System.currentTimeMillis();

        Thread t = new Thread(new Submain(numberOfPhilosophers, thinkingTime, eatingTime));
        t.start();

        System.out.println("press Enter Key to stop program");
        sc.nextLine();
        sc.nextLine();
        t.interrupt();

        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println("total runtime in ms: " + elapsedTime);
    }
}
