package lamponi.at;

import java.util.concurrent.Semaphore;

public class ForkSynchronized extends Fork {
    private Semaphore semaphore = new Semaphore(1);

    ForkSynchronized(int ID) {
        super(ID);
    }

    public boolean take(Philosopher phil) throws InterruptedException {
        semaphore.acquire();
        this.phil = phil;
        System.out.println(phil.getName() + " picks up fork " + ID);
        return true;
    }

    public boolean put(Philosopher phil)
    {
        System.out.println(phil.getName() + " puts fork " + ID + " down");
        this.phil = new Philosopher("none");
        semaphore.release();
        return false; // fork is no longer in his hand
    }
}
