package lamponi.at;

public class SmartPhilosopher extends Philosopher {

    private int ID;

    SmartPhilosopher(String name, int ID) {
        super(name);
        this.ID = ID;
    }

    SmartPhilosopher(String name, Fork forkLeft, Fork forkRight, int thinkingTime, int eatingTime, int roundsTillStarvation, int ID) {
        super(name, forkLeft, forkRight, thinkingTime, eatingTime, roundsTillStarvation);
        this.ID = ID;
    }

    protected void pickUpForks() throws InterruptedException {
        if(ID % 2 == 0)
        {
            if(!hasForkRight)
            {
                hasForkRight = forkRight.take(this);
            }

            if(!hasForkLeft && hasForkRight)
            {
                hasForkLeft = forkLeft.take(this);
            }
        }
        else
        {
            if(!hasForkLeft)
            {
                hasForkLeft = forkLeft.take(this);
            }

            if(!hasForkRight && hasForkLeft)
            {
                hasForkRight = forkRight.take(this);
            }
        }
    }
}
