package lamponi.at;

public class Submain implements Runnable
{
    private int numberOfPhilosophers;
    private int thinkingTime;
    private int eatingTime;
    private int roundsTillStarvation = 5;

    Submain(int numberOfPhilosophers, int thinkingTime, int eatingTime) {
        this.numberOfPhilosophers = numberOfPhilosophers;
        this.thinkingTime = thinkingTime;
        this.eatingTime = eatingTime;
    }

    @Override
    public void run() {
        //starvingPhilosophers();
        //deadlockPhilosophersSynchronized();
        smartPhilosophersSynchronized();
    }

    private void starvingPhilosophers()
    {
        Dinner dinnerDeadlockSimple = new Dinner(numberOfPhilosophers, thinkingTime, eatingTime, roundsTillStarvation);
        dinnerDeadlockSimple.run();
        System.out.println("time need for execution: " + dinnerDeadlockSimple.getElapsedTime());
        System.out.println();
    }

    private void deadlockPhilosophersSynchronized() //deadlocks
    {
        Dinner dinnerDeadlockSynchronized = new DinnerSynchronized(numberOfPhilosophers, thinkingTime, eatingTime, roundsTillStarvation);
        dinnerDeadlockSynchronized.run();
        System.out.println("time need for execution: " + dinnerDeadlockSynchronized.getElapsedTime());
        System.out.println();
    }

    private void smartPhilosophersSynchronized() //runs
    {
        Dinner dinnerDeadlockSynchronized = new DinnerSmartPhils(numberOfPhilosophers, thinkingTime, eatingTime, roundsTillStarvation);
        dinnerDeadlockSynchronized.run();
        System.out.println("time need for execution: " + dinnerDeadlockSynchronized.getElapsedTime());
        System.out.println();
    }
}