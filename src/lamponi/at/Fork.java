package lamponi.at;

public class Fork {
    Philosopher phil = new Philosopher("none");
    int ID;

    Fork(int ID)
    {
        this.ID = ID;
    }

    public int getID()
    {
        return this.ID;
    }

    public boolean take(Philosopher phil) throws InterruptedException {
        if (this.phil.getName().equals("none"))
        {
            this.phil = phil;
            System.out.println(phil.getName() + " picks up fork " + ID);
            return true;
        }
        else if(this.phil.getName().equals(phil.getName()))
        {
            System.out.println(phil.getName() + " already has fork " + ID);
            return true;
        }
        else
        {
            System.out.println(phil.getName() + " cannot take fork because " + this.phil.getName() + " has fork " + ID);
            return false;
        }
    }

    public boolean put(Philosopher phil) throws RuntimeException {
        if(this.phil.getName().equals(phil.getName()))
        {
            System.out.println(phil.getName() + " puts fork " + ID + " down");
            this.phil = new Philosopher("none");
        }
        else
        {
            System.out.println(phil.getName() + " cannot put down someone else's fork ( " + ID + " )!");
            throw new RuntimeException();
        }
        return false; //fork is no longer in hand
    }
}
